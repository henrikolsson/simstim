#version 140
in vec2 tv;
uniform vec4 color;
out vec4 finalColor;
uniform sampler2D tex;

void main(void) {
    finalColor = vec4(1, 1, 1,
                      texture2D(tex, tv).r) * color;
};
