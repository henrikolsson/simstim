#version 140
uniform mat4 mvp;
in vec4 vert;
out vec4 tv;
void main(void) {
    tv = vert;
    gl_Position = mvp * vec4(vert.xyz, 1);
}
