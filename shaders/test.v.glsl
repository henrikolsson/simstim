#version 140 
#extension GL_ARB_explicit_attrib_location : enable
layout(location = 0) in vec3 vert;
layout(location = 1) in int material;

uniform mat4 mvp;
out vec3 tv;
flat out int mt;

void main(void) {
    tv = vert;
    mt = material;
    gl_Position = mvp * vec4(vert.xyz, 1);
}
