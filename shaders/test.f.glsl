#version 140
out vec4 finalColor;
in vec3 tv;
flat in int mt;
uniform sampler2D texture;
const vec4 fogcolor = vec4(0.6, 0.8, 1.0, 1.0);
const float fogdensity = .00003;
const float size = 16.0;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void) {
    
    vec2 coord2d;
    float intensity;
    if(mt < 0) {
        coord2d = vec2((fract(tv.x) + mt) / size, tv.z);
        intensity = 1.0;
    }
    else {
        coord2d = vec2((fract(tv.x + tv.z) + mt) / size, -tv.y);
        intensity = 0.85;
    }
    vec4 color =  texture2D(texture, coord2d) * intensity;


    /* if (mt == 1) */
    /*     finalColor = vec4(0.0, 0.0, 1.0, 1.0); */
    /* else if (mt == 2) */
    /*     finalColor = vec4(0.0, 1.0, 0.0, 1.0); */
    /* else */
    /*     finalColor = vec4(1.0, 0.0, 0.0, 1.0); */

    /* if (mt == 1) */
    /*     finalColor = vec4(1.0, 0.0, 0.0, 1.0); */
    /* else if (mt == 2) */
    /*     finalColor = vec4(0.0, 0.0, 1.0, 1.0); */
    /* else if (mt == 3) */
    /*     finalColor = vec4(0.0, 1.0, 0.0, 1.0); */
    /* else if (mt == 4) */
    /*     finalColor = vec4(0.0, 1.0, 0.5, 1.0); */
    /* else */
    /*     finalColor = vec4(1.0, 0.0, 0.5, 1.0); */
    
    //finalColor = vec4(tv.y / 128.0, tv.y / 256.0, tv.y / 512.0, 1.0);
    //finalColor = vec4(1.0, 0.0, 0.0, 1.0);
        
    float z = gl_FragCoord.z / gl_FragCoord.w;
    float fog = clamp(exp(-fogdensity * z * z), 0.2, 1);
    finalColor = mix(fogcolor, color, fog);

};
