#version 140
in vec4 vert;
out vec2 tv;
void main(void) {
    gl_Position = vec4(vert.xy, 0, 1);
    tv = vert.zw;
}
