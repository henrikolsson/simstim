with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "lark";
  buildInputs = [ clang cmake bear openal glfw3 pkgconfig x11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.xinput xorg.libXi vulkan-headers glew spdlog libGLU protobuf xorg.libpthreadstubs enet glog glm gflags gtest ];
}
