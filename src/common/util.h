#ifndef _UTIL_H
#define _UTIL_H

#include <enet/enet.h>
#include <glog/logging.h>
#include <time.h>
#include <glm/glm.hpp>
#include "../proto/simstim.pb.h"

#define CHUNK_IDX(x,y,z) ((x) + ((y)*CHUNK_SIZE) + ((z)*CHUNK_SIZE * CHUNK_SIZE))
#define CHUNK_IDX_SLAB(x,y,z) (((z) * CHUNK_SIZE + (y)) * CHUNK_SIZE + (x))

typedef struct {
    unsigned long size,resident,share,text,lib,data,dt;
} statm_t;

void get_memory_stats(statm_t& result);
unsigned long get_resident_memory();
double get_millis();
uint64_t get_nanos();
void initCommon();
void deinitCommon();
Vec3 *glmToProto(glm::vec3 vec);
glm::vec3 protoToGlm(const Vec3 *vec);

#endif
