#include <memory>
#include <fstream>

using namespace std;

const string BASE_PATH = "/home/henrik/dev/simstim/assets/";

unique_ptr<string> readFile(string &filename);

unique_ptr<string> readFile(string &filename)
{
    unique_ptr<string> result(new string());
    
    ifstream in(BASE_PATH + filename);
    if (!in.is_open())
        throw runtime_error("unable to open file: " + BASE_PATH + filename);

    result->append(istreambuf_iterator<char>(in),
                   istreambuf_iterator<char>());
    
    return result;
}
