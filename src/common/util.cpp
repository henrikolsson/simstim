#include "util.h"

static uint64_t _start_time = 0;
static bool initialized = false;

double get_millis()
{
    return static_cast<double>(get_nanos() - _start_time) / static_cast<uint64_t>(100000);
}

uint64_t get_nanos()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC,&ts);
    uint64_t t = static_cast<uint64_t>(ts.tv_sec) *static_cast<uint64_t>(1000000000) + static_cast<uint64_t>(ts.tv_nsec);
    return t;
}

void initCommon()
{
    if (!initialized)
    {
        google::InitGoogleLogging("");
        google::InstallFailureSignalHandler();
        enet_initialize();
        initialized = true;
        _start_time = get_nanos();
        LOG(INFO) << "Common initialized: " << _start_time;
    }
}

void deinitCommon()
{
    LOG(INFO) << "Common deinitialized";
    if (initialized)
    {
        enet_deinitialize();
    }
}

Vec3 *glmToProto(glm::vec3 vec)
{
    Vec3 *r = new Vec3();
    r->set_x(vec.x);
    r->set_y(vec.y);
    r->set_z(vec.z);
    return r;
}

glm::vec3 protoToGlm(const Vec3 *vec)
{
    return glm::vec3(vec->x(), vec->y(), vec->z());
}

void get_memory_stats(statm_t& result)
{
    const char* statm_path = "/proc/self/statm";

    FILE *f = fopen(statm_path,"r");
    if(!f){
        perror(statm_path);
        abort();
    }
    if(7 != fscanf(f,"%lu %lu %lu %lu %lu %lu %lu",
                   &result.size,&result.resident,&result.share,&result.text,&result.lib,&result.data,&result.dt))
    {
        perror(statm_path);
        abort();
    }
    fclose(f);
}

unsigned long get_resident_memory()
{
    statm_t stat;
    get_memory_stats(stat);
    return stat.resident;
}
