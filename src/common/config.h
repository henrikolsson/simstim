#ifndef _CONFIG_H
#define _CONFIG_H

#define CHUNK_SIZE 16

#define VIEWPORT_X 16
#define VIEWPORT_Y 4
#define VIEWPORT_Z 16

//#define MOCK_VOXELS

#endif
