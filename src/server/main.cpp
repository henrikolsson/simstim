#include "../net/server.h"

int main(int argc, char *argv[])
{
    const char *host;
    int port;
    Server *server = nullptr;
    if (argc > 2)
    {
        host = argv[1];
        port = atoi(argv[2]);
        CHECK(port > 0) << "Invalid port specified: " << argv[2];
        server = new Server(host, port);
    }
    else
    {
        server = new Server();
    }
    LOG(INFO) << "Running on port: " << server->getPort();
    server->run();
}
