#include <gtest/gtest.h>
#include <glog/logging.h>
#include "../net/server.h"
#include "../net/client.h"
#include "../common/util.h"
#include "../client/world.h"
#include "../client/chunk.h"

TEST(ServerClientTest, CanStartServer) {
    initCommon();
    Server server;
    server.runAsync();
    server.stop();
    //EXPECT_EQ(1, 2);
}

TEST(ServerClientTest, CanConnectToServer) {
    initCommon();
    Server server("127.0.0.1", 1312);
    server.runAsync();

    Client client;
    EXPECT_TRUE(client.connect("127.0.0.1", 1312));

    server.stop();
}

TEST(ServerClientTest, CanSayHello) {
    initCommon();
    Server server("127.0.0.1", 1312);
    server.runAsync();

    Client client;
    EXPECT_TRUE(client.connect("127.0.0.1", 1312));

    client.hello();
    for (int i=0;i<20;i++)
    {
        client.work();
        usleep(100000);
        if (client.packets.size() > 0)
        {
            goto success;
        }
    }
    ADD_FAILURE();
    
    success:
    server.stop();
}

TEST(UtilTest, CanConvertBetweenProtoAndGlm) {
    initCommon();

    glm::vec3 foo(1.5, 3.2, 1.9);
    Vec3 *res = glmToProto(foo);
    EXPECT_EQ(res->x(), foo.x);
    EXPECT_EQ(res->y(), foo.y);
    EXPECT_EQ(res->z(), foo.z);

    foo = protoToGlm(res);
    EXPECT_EQ(res->x(), foo.x);
    EXPECT_EQ(res->y(), foo.y);
    EXPECT_EQ(res->z(), foo.z);
}

TEST(WorldTest, CanGetCorrectBlocks) {
    initCommon();
    World world;

    double data[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
    for (int x=0;x<CHUNK_SIZE;x++)
    {
        for (int y=0;y<CHUNK_SIZE;y++)
        {
            for (int z=0;z<CHUNK_SIZE;z++)
            {
                data[CHUNK_IDX(x, y, z)] = -5.0;
            }
        }
    }
        
    std::string s(reinterpret_cast<char *>(data), sizeof(data));
    world.setChunkAt(0,0,0, s);
    
    data[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
    for (int x=0;x<CHUNK_SIZE;x++)
    {
        for (int y=0;y<CHUNK_SIZE;y++)
        {
            for (int z=0;z<CHUNK_SIZE;z++)
            {
                data[CHUNK_IDX(x, y, z)] = -8.0;
            }
        }
    }
    s = std::string(reinterpret_cast<char *>(data), sizeof(data));
    world.setChunkAt(-1,0,0, s);
    
    data[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
    for (int x=0;x<CHUNK_SIZE;x++)
    {
        for (int y=0;y<CHUNK_SIZE;y++)
        {
            for (int z=0;z<CHUNK_SIZE;z++)
            {
                data[CHUNK_IDX(x, y, z)] = -9.0;
            }
        }
    }
    s = std::string(reinterpret_cast<char *>(data), sizeof(data));
    world.setChunkAt(-2,0,0, s);
    
    EXPECT_EQ(-5.0, world.getBlock(5,5,5));
    EXPECT_EQ(-8.0, world.getBlock(-10,0,0));
    EXPECT_EQ(-9.0, world.getBlock(-20,0,0));
    EXPECT_EQ(DEFAULT_BLOCK_VALUE, world.getBlock(-33,0,0));
}
