#ifndef _CAMERA_H
#define _CAMERA_H

#include <glm/glm.hpp>

static const float mousespeed = 0.001;

class Camera
{
 public:
    Camera();
    ~Camera();
    glm::vec3 getPosition();
    void setPosition(glm::vec3 position);
    glm::vec3 getLookat();
    glm::vec3 getAngle();
    void setAngle(glm::vec3 angle);
    glm::vec3 getUp();
    glm::vec3 getForwardDirection();
    glm::vec3 getRightDirection();    
    void look(double x, double y);
    void tryMove(glm::vec3 dir);
 private:
    void updateVectors();
    glm::vec3 lookat;
    glm::vec3 position;
    glm::vec3 angle;
    glm::vec3 forward;
    glm::vec3 right;
    glm::vec3 up;
};

#endif

