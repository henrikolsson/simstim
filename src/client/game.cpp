#include "game.h"

Game::Game(const char *host, int port)
{
    m_client.setCallbacks(this);
    if (!m_client.connect(host, port))
    {
        throw std::runtime_error("Failed to connecct to server");
    }
}

Game::~Game()
{
}

void Game::start()
{
    glfwSetErrorCallback(&glfwError);
    if (glfwInit() == GL_FALSE)
    {
        throw std::runtime_error("Failed to initialize GLFW");
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    m_windowWidth = DEFAULT_WINDOW_WIDTH;
    m_windowHeight = DEFAULT_WINDOW_HEIGHT;
    m_window = glfwCreateWindow(DEFAULT_WINDOW_WIDTH,
                                DEFAULT_WINDOW_HEIGHT,
                                "simstim", NULL, NULL);
    // m_input = new Input(this);
    if (!m_window)
    {
        glfwTerminate();
        throw std::runtime_error("Failed to create window");
    }
    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    glfwSetWindowUserPointer(m_window, this);
    glfwSetCursorPosCallback(m_window, &glfwCursorPos);
    glfwSetKeyCallback(m_window, &glfwKey);
    glfwSetWindowSizeCallback(m_window, &glfwSetWindowSize2);
    glfwSetWindowFocusCallback(m_window, &glfwWindowFocus);
    glfwSetMouseButtonCallback(m_window, &glfwMouseButton);

    glfwMakeContextCurrent(m_window);
    CHECK_GL_ERRORS();
    
    glewExperimental = GL_TRUE;
    GLenum glewStatus = glewInit();
    if (glewStatus != GLEW_OK)
    {
        glfwTerminate();
        throw std::runtime_error("GLEW Error: " +
                                 std::string(reinterpret_cast<const char *>(glewGetErrorString(glewStatus))));
    }
    // Ignore harmless GL_INVALID_ENUM from glewInit
    glGetError();

    LOG(INFO) << "OpenGL version string: " << glGetString(GL_VERSION); 
    LOG(INFO) << "OpenGL renderer string: " << glGetString(GL_RENDERER);
    LOG(INFO) << "OpenGL vendor string: " << glGetString(GL_VENDOR);
    CHECK_GL_ERRORS();

    program = new Program();
    program->attachShader("test.v.glsl", GL_VERTEX_SHADER);
    program->attachShader("test.f.glsl", GL_FRAGMENT_SHADER);
    program->link();
    
    CHECK_GL_ERRORS();

    text = new Text();

    m_scene.push_back(new Entity(1, program));
    m_scene.push_back(new Entity(2, program));

    m_client.hello();
    // m_client.getChunk(-5,-5,-5);
    // m_client.getChunk(-5,-5,5);
    // m_client.getChunk(5,-5,-5);
    // m_client.getChunk(5,5,5);
    // m_client.getChunk(-5,-0,-5);
    // m_client.getChunk(-1,0,0);
    // m_client.getChunk(5,0,5);
    
    program->use();
    m_me.setPosition(glm::vec3(7,7,7));
    GLint uniform_mvp = program->getUniform("mvp");
    glm::mat4 model = glm::mat4(1);
    glm::mat4 view =  glm::lookAt(m_me.getPosition(),
                                  glm::vec3(0.0, 0.0, 0.0),
                                  glm::vec3(0.0, 1.0, 0.0));
    glm::mat4 projection = glm::perspective(45.0f,
                                            1.0f*m_windowWidth/m_windowHeight,
                                            0.1f, 10000000.0f);
    m_mvp = projection * view * model;
    glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(m_mvp));
    CHECK_GL_ERRORS();

    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    
    glCullFace(GL_BACK);
    glFrontFace(GL_CW);

    CHECK_GL_ERRORS();

    GLint max_layers;
    glGetIntegerv (GL_MAX_ARRAY_TEXTURE_LAYERS, &max_layers);
    LOG(INFO) << "max layers: " << max_layers;    
    this->run();
}

Entity *Game::entityById(int32_t id)
{
    for (Entity *e : m_scene)
    {
        if (e->getId() == id)
            return e;
    }
    return NULL;
}

void Game::run()
{
    double fps_start = glfwGetTime();
    int frames = 0;
    int fps = 0;
    while (!glfwWindowShouldClose(m_window))
    {
        double ttt = glfwGetTime() - fps_start;
        if (ttt > 1)
        {
            fps = static_cast<int>(frames / ttt);
            frames = 0;
            fps_start = glfwGetTime();
        }

        m_client.setInput(m_move);
        m_client.setForward(m_camera.getForwardDirection());
        m_client.setRight(m_camera.getRightDirection());
        m_client.work();
        glClearColor(0.0, 0, 0, 1); // black
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        program->use();

        // double d = 1;
        // for (Entity *e : m_scene)
        // {
        //     GLint uniform_mvp = program->getUniform("mvp");
        //     glm::mat4 model = glm::mat4(1);
        //     glm::mat4 view =  glm::lookAt(m_me.getPosition(),
        //                                   glm::vec3(0.0, 0.0, 0.0),
        //                                   glm::vec3(0.0, 1.0, 0.0));
        //     glm::mat4 projection = glm::perspective(45.0f,
        //                                             1.0f*m_windowWidth/m_windowHeight,
        //                                             0.1f, 10.0f);
        //     m_mvp = projection * view * model;
            
        //     glm::mat4 mat = m_mvp * glm::translate(glm::mat4(1),
        //                                            glm::ve                                      c3(1, d, d));
        //     glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(mat));
            
        //     e->render();
            
        //     d = d * 4;
        // }
        CHECK_GL_ERRORS();

        
        int32_t cx = W_INDEX_TO_C_INDEX(m_me.getPosition().x);
        int32_t cy = W_INDEX_TO_C_INDEX(m_me.getPosition().y);
        int32_t cz = W_INDEX_TO_C_INDEX(m_me.getPosition().z);
        
        GLint uniform_mvp = program->getUniform("mvp");
        glm::mat4 model = glm::mat4(1);
        glm::mat4 view =  glm::lookAt(m_me.getPosition(),
                                      m_me.getPosition() + m_camera.getLookat(),
                                      m_camera.getUp());
        glm::mat4 projection = glm::perspective(45.0f,
                                                1.0f*m_windowWidth/m_windowHeight,
                                                0.1f, 10000000.0f);
        m_mvp = projection * view * model;
        glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(m_mvp));
        CHECK_GL_ERRORS();
        int vertices = 0;
        for (int32_t x=cx-VIEWPORT_X/2;x<cx+VIEWPORT_X/2+1;x++)
        {
            for (int32_t y=cy-VIEWPORT_Y/2;y<cy+VIEWPORT_Y/2+1;y++)
            {
                for (int32_t z=cz-VIEWPORT_Z/2;z<cz+VIEWPORT_Z/2+1;z++)
                {
                    Chunk *chunk = m_world.getChunkAt(x, y, z);
                    if (chunk != nullptr)
                    {
                        GLint uniform_mvp = program->getUniform("mvp");
                        glm::mat4 mat = m_mvp * glm::translate(glm::mat4(1),
                                                               glm::vec3(chunk->getWorldX(),
                                                                         chunk->getWorldY(),
                                                                         chunk->getWorldZ()));
                        glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, glm::value_ptr(mat));

                        chunk->render(program);
                        vertices += chunk->numberOfVertices();
                        CHECK_GL_ERRORS();
                    }
                }
            }
        }
        CHECK_GL_ERRORS();

        cx = W_INDEX_TO_C_INDEX(m_me.getPosition().x);
        cy = W_INDEX_TO_C_INDEX(m_me.getPosition().y);
        cz = W_INDEX_TO_C_INDEX(m_me.getPosition().z);
        
        std::stringstream str_stream;
        str_stream << "FPS: " << fps << " memory: " << static_cast<int>(get_resident_memory() / 1048576.0 * 4096) << "MB";
        text->addLine(str_stream.str());

        str_stream.str(std::string());
        str_stream << "position: " << m_me.getPosition().x << ", " << m_me.getPosition().y << ", " << m_me.getPosition().z;
        text->addLine(str_stream.str());
        
        str_stream.str(std::string());
        str_stream << "chunk: " << cx << ", " << cy << ", " << cz;
        text->addLine(str_stream.str());

        str_stream.str(std::string());
        str_stream << "chunks loaded: " << m_world.loadedChunks();
        text->addLine(str_stream.str());

        str_stream.str(std::string());
        str_stream << "vertices: " << vertices;
        text->addLine(str_stream.str());

        text->render();
        glfwSwapBuffers(m_window);
        glfwPollEvents();
         
        CHECK_GL_ERRORS();
         
        if (glfwGetKey(m_window , GLFW_KEY_ESCAPE) == GLFW_PRESS)
            break;
        frames++;
    }
}

void Game::clientChunkData(ChunkData *cd)
{
    assert(cd->data().size() == (CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * sizeof(char)));
    m_world.setChunkAt(cd->cx(), cd->cy(), cd->cz(), static_cast<const std::string&>(cd->data()));
}

void Game::clientSetPosition(SetPosition *sp)
{
    if (sp->entityid() < 0)
    {
        int32_t cx = W_INDEX_TO_C_INDEX(sp->position().x());
        int32_t cy = W_INDEX_TO_C_INDEX(sp->position().y());
        int32_t cz = W_INDEX_TO_C_INDEX(sp->position().z());
        for (int32_t x=cx-VIEWPORT_X/2;x<cx+VIEWPORT_X/2+1;x++)
        {
            for (int32_t y=cy-VIEWPORT_Y/2;y<cy+VIEWPORT_Y/2+1;y++)
            {
                for (int32_t z=cz-VIEWPORT_Z/2;z<cz+VIEWPORT_Z/2+1;z++)
                {
                    if (m_world.getChunkAt(x, y, z) == nullptr && y > -1)
                    {
#ifdef MOCK_VOXELS
                        if (x == 0 && y == 0 && z == 0)
#endif
                            m_client.getChunk(x, y, z);
                    }
                }
            }
        }
        
        m_me.setPosition(protoToGlm(&(sp->position())));        
    }
    else
    {
        LOG(INFO) << "Other pos";
    }
}

void Game::cursor(double x, double y)
{
    x = (x - m_windowWidth / 2);
    y = (y - m_windowHeight / 2);
    glfwSetCursorPos(m_window,
                     m_windowWidth / 2,
                     m_windowHeight / 2);
    m_camera.look(x, y);
}

void Game::key(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        if(key == GLFW_KEY_A)
            m_move |= PROTO_MOVE_DIRECTION_LEFT;     
        if(key == GLFW_KEY_D)
            m_move |= PROTO_MOVE_DIRECTION_RIGHT;
        if(key == GLFW_KEY_W)
            m_move |= PROTO_MOVE_DIRECTION_FORWARD;     
        if(key == GLFW_KEY_S)
            m_move |= PROTO_MOVE_DIRECTION_BACKWARD;     
        if(key == GLFW_KEY_E)
            m_move |= PROTO_MOVE_DIRECTION_UP;     
        if(key == GLFW_KEY_Q)
            m_move |= PROTO_MOVE_DIRECTION_DOWN;

        if (key == GLFW_KEY_1)
            m_world.toggleMeshExtractor();
        
    }
    else if (action == GLFW_RELEASE)
    {
        if(key == GLFW_KEY_A)
            m_move &= ~PROTO_MOVE_DIRECTION_LEFT;     
        if(key == GLFW_KEY_D)
            m_move &= ~PROTO_MOVE_DIRECTION_RIGHT;
        if(key == GLFW_KEY_W)
            m_move &= ~PROTO_MOVE_DIRECTION_FORWARD;     
        if(key == GLFW_KEY_S)
            m_move &= ~PROTO_MOVE_DIRECTION_BACKWARD;     
        if(key == GLFW_KEY_E)
            m_move &= ~PROTO_MOVE_DIRECTION_UP;
        if(key == GLFW_KEY_Q)
            m_move &= ~PROTO_MOVE_DIRECTION_DOWN;
    }    
}
