#ifndef _TYPES_H
#define _TYPES_H

#include <glm/glm.hpp>
#include <GL/glew.h>

typedef glm::tvec4<GLbyte> byte4;
typedef glm::tvec4<GLfloat> float4;

#endif
