#ifndef _GUTIL_H
#define _GUTIL_H

#include <GL/glew.h>
#include <stdexcept>
#include <glog/logging.h>

const char *glErrorToStr(GLenum err);

#ifndef NDEBUG
#define CHECK_GL_ERRORS() do { bool has_err=false; GLenum err; while ((err = glGetError()) != GL_NO_ERROR) { has_err=true; LOG(ERROR) << "GL Error " << err << ": " << glErrorToStr(err); } if (has_err) throw std::runtime_error("GL Error occured"); } while (0)
#else
#define CHECK_GL_ERRORS()
#endif

#endif
