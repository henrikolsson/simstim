#include "camera.h"

Camera::Camera()
{
    position = glm::vec3(-44.0, 20.0, 2.0);
    angle = glm::vec3(0.772367, -0.032793, 0.000000);
    updateVectors();
}

Camera::~Camera()
{
}

glm::vec3 Camera::getPosition()
{
    return position;
}

glm::vec3 Camera::getLookat()
{
    return lookat;
}

void Camera::setAngle(glm::vec3 angle)
{
    this->angle = angle;
    this->updateVectors();
}

void Camera::look(double x, double y)
{
    angle.x -= x * mousespeed;
    angle.y -= y * mousespeed;
    
    if(angle.x < -M_PI)
        angle.x += M_PI * 2;
    if(angle.x > M_PI)
        angle.x -= M_PI * 2;
    if(angle.y < -M_PI / 2)
        angle.y = -M_PI / 2;
    if(angle.y > M_PI / 2)
        angle.y = M_PI / 2;

    this->updateVectors();
}

void Camera::tryMove(glm::vec3 dir)
{
    glm::vec3 pos = position + dir;
    dir.y -= 1.0;
    //if (!engine->getWorld()->is_blocked(pos + dir))
    setPosition(pos);
}
    
void Camera::updateVectors()
{
    forward.x = sinf(angle.x);
    forward.y = 0;
    forward.z = cosf(angle.x);

    right.x = -cosf(angle.x);
    right.y = 0;
    right.z = sinf(angle.x);

    lookat.x = sinf(angle.x) * cosf(angle.y);
    lookat.y = sinf(angle.y);
    lookat.z = cosf(angle.x) * cosf(angle.y);

    up = glm::cross(right, lookat);
}

glm::vec3 Camera::getForwardDirection()
{
    return forward;
}

glm::vec3 Camera::getRightDirection()
{
    return right;
}

glm::vec3 Camera::getAngle()
{
    return angle;
}

glm::vec3 Camera::getUp()
{
    return up;
}

void Camera::setPosition(glm::vec3 position)
{
    this->position = position;
}
