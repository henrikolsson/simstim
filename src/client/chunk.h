#ifndef _CHUNK_H
#define _CHUNK_H

#include <string.h>
#include <cstdint>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "types.h"
#include "program.h"
#include "../common/config.h"
#include "../common/util.h"
#include "../common/mc.h"
#include "terrain/mesh.h"

class World;
class MeshExtractor;

class Chunk
{
 public:
    Chunk(int32_t cx, int32_t cy, int32_t cz, World *world);
    ~Chunk();
    int32_t getWorldX();
    int32_t getWorldY();
    int32_t getWorldZ();
    int32_t getChunkX();
    int32_t getChunkY();
    int32_t getChunkZ();
    void setData(const std::string &data);
    void render(Program *program);
    void initialize2(Program *program);
    void initialize(Program *program);
    bool isAir(int32_t x, int32_t y, int32_t z);
    bool isTransparent(int32_t x, int32_t y, int32_t z);
    uint64_t getLastUsed();
    void toggleMeshExtractor();
    float getBlock(int32_t x, int32_t y, int32_t z);
    void initialize3(Program *program) ;
    int numberOfVertices();
 private:
    World *m_world;
    int32_t m_cx;
    int32_t m_cy;
    int32_t m_cz;
    char m_data[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
    bool m_initialized = false;
    GLuint m_vbo;
    GLuint m_idx;
    int m_elements;
    uint64_t m_lastUsed = get_nanos();
    bool m_useMarchingCubes;
    std::vector<unsigned int> m_indices;
    Mesh *mesh = nullptr;
    int currentME = 0;
};

#endif
