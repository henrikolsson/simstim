#ifndef _TEXT_H
#define _TEXT_H

#include <string>
#include <stdexcept>
#include <vector>

#include <GL/glew.h>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "program.h"

class Engine;

class Text
{
 public:
    Text();
    ~Text();
    void render();
    void addLine(std::string line);
 private:
    std::vector<std::string> lines;
    FT_Library ft;
    FT_Face face;
    FT_GlyphSlot g;
    GLuint tex;
    GLint uniform_tex;
    GLint uniform_color;
    GLint attribute_coord;
    Program program;
    GLuint vbo;
};
#endif
