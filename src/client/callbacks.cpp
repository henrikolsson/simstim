#include "game.h"

void glfwCursorPos(GLFWwindow *window, double x, double y)
{
    Game *game = static_cast<Game*>(glfwGetWindowUserPointer(window));
    game->cursor(x, y);
}

void glfwKey(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    Game *game = static_cast<Game*>(glfwGetWindowUserPointer(window));
    game->key(key,scancode,action,mods);
    // game->getInput()->key(key, scancode, action, mods);
}

void glfwSetWindowSize2(GLFWwindow *window, int width, int height)
{
    Game *game = static_cast<Game*>(glfwGetWindowUserPointer(window));
    // game->windowSizeChanged(width, height);
}

void glfwError(int code, const char *error)
{
    LOG(ERROR) << "GLFW error 0x" << std::hex << code << ": " << error;
    abort();
}

void glfwWindowFocus(GLFWwindow *window, int focused)
{
    Game *game = static_cast<Game*>(glfwGetWindowUserPointer(window));
    // game->windowFocusChanged(focused == GL_TRUE);
}

void glfwMouseButton(GLFWwindow *window, int button, int action, int mods)
{
    Game *game = static_cast<Game*>(glfwGetWindowUserPointer(window));
    // game->getInput()->mouse(button, action, mods);
}
