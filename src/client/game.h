#ifndef _GAME_H
#define _GAME_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glog/logging.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>
#include <cstdint>
#include "../net/client.h"
#include "../net/client_callbacks.h"
#include "callbacks.h"
#include "gutil.h"
#include "program.h"
#include "entity.h"
#include "world.h"
#include "camera.h"
#include "text.h"

using std::int32_t;


#define DEFAULT_WINDOW_WIDTH 1024
#define DEFAULT_WINDOW_HEIGHT 768
class Game : public ClientCallbacks
{
 public:
    Game(const char *host, int port);
    ~Game();
    void start();
    void run();
    void clientSetPosition(SetPosition *sp);
    void clientChunkData(ChunkData *cd);
    void key(int key, int scancode, int action, int mods);
    void cursor(double x, double y);
 private:
    Program *program;
    Client m_client;
    int m_windowWidth;
    int m_windowHeight;
    GLFWwindow* m_window;
    std::vector<Entity*> m_scene;
    Entity *entityById(int32_t id);
    glm::mat4 m_mvp;
    Entity m_me = Entity(-1, nullptr);
    int32_t m_move = 0;
    World m_world;
    Camera m_camera;
    Text *text;
};

#endif
