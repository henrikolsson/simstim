#include "game.h"
#include "../net/server.h"
#include "../net/client.h"

int main(int argc, char *argv[])
{
    initCommon();
    
    const char *host;
    int port;
    Server *server = nullptr;
    if (argc > 2)
    {
        host = argv[1];
        port = atoi(argv[2]);
        CHECK(port > 0) << "Invalid port specified: " << argv[2];
    }
    else
    {
        server = new Server();
        server->runAsync();
        host = "localhost";
        port = server->getPort();
    }

    Game game(host, port);
    game.start();
    
    if (server != nullptr)
        delete server;

    deinitCommon();
}
