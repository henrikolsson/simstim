#include "world.h"

World::World()
{
    
}
void World::toggleMeshExtractor()
{
    m_marchingCubes = !m_marchingCubes;
    LOG(INFO) << "Toggle mesh extractor: " << m_marchingCubes;
    for (auto c : m_chunks)
    {
        c->toggleMeshExtractor();
    }
}

Chunk *World::getChunkAt(int32_t cx, int32_t cy, int32_t cz)
{
    //LOG(INFO) << "getChunkAt: " << cx << ", " << cy << ", " << cz;
    for (auto c : m_chunks)
    {
        if (c->getChunkX() == cx &&
            c->getChunkY() == cy &&
            c->getChunkZ() == cz)
        {
            return c;
        }
    }
    return nullptr;
}

float World::getBlock(int ax, int ay, int az)
{
    int cix = static_cast<int>(floor(ax / static_cast<float>(CHUNK_SIZE)));
    int ciy = static_cast<int>(floor(ay / static_cast<float>(CHUNK_SIZE)));
    int ciz = static_cast<int>(floor(az / static_cast<float>(CHUNK_SIZE)));
    Chunk *c = getChunkAt(cix,
                          ciy,
                          ciz);
    VLOG(1) << "ax: " << ax << " ay: " << ay << " az: " << az;
    VLOG(1) << "cix: " << cix << " ciy: " << ciy << " ciz: " << ciz;
     if (c == nullptr)
     {
         VLOG(1) << "no chunk at x: " << ax << " y: " << ay << " z:" << az;
         return -1;
     }

    int cx = abs(ax) % CHUNK_SIZE;
    int cy = abs(ay) % CHUNK_SIZE;
    int cz = abs(az) % CHUNK_SIZE;
    if (ax < 0 && cx != 0)
        cx = CHUNK_SIZE - cx;
    if (ay < 0 && cy != 0)
        cy = CHUNK_SIZE - cy;
    if (az < 0 && cz != 0)
        cz = CHUNK_SIZE - cz;

    VLOG(1) << "cx: " << cx << " cy: " << cy << " cz: " << cz;
    return c->getBlock(cx, cy, cz);
}
/*
  -16 = -2
  -5 = -1
   0 = 0
   5 = 0
   16 = 1
  
*/  
void World::setChunkAt(int32_t cx, int32_t cy, int32_t cz, const std::string &data)
{
    LOG(INFO) << "SET CHUNK";
    Chunk *c = new Chunk(cx, cy, cz, this);
    c->setData(data);
    m_chunks.push_back(c);
}

int World::loadedChunks()
{
    return m_chunks.size();
}

std::vector<Chunk *> World::getChunks()
{
    return m_chunks;
}
