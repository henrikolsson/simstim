#ifndef _MESH_EXTRACTOR_H
#define  _MESH_EXTRACTOR_H

#include "mesh.h"

class Chunk;

class MeshExtractor
{
  public:
    virtual Mesh *extract(Chunk *chunk) = 0;
};

#endif
