#ifndef _SN_MESH_EXTRACTOR_H
#define _SN_MESH_EXTRACTOR_H

#include "mesh_extractor.h"
#include "mesh.h"

class SnMeshExtractor : MeshExtractor
{
  public:
    Mesh *extract(Chunk *chunk);
};

#endif
