#include "sn_mesh_extractor.h"
#include "../chunk.h"

static inline int offset_3d_slab(const glm::vec3 &p, const glm::vec3 &size)
{
    return size.x * size.y * ((int)p.z % 2) + p.y * size.x + p.x;
}

static void quad(Mesh *mesh, Chunk *chunk, bool flip, int ia, int ib, int ic, int id)
{
	if (flip)
		std::swap(ib, id);

	Vertex &va = mesh->vertices[ia];
	Vertex &vb = mesh->vertices[ib];
	Vertex &vc = mesh->vertices[ic];
	Vertex &vd = mesh->vertices[id];

	const glm::vec3 ab = va.position - vb.position;
	const glm::vec3 cb = vc.position - vb.position;
	const glm::vec3 n1 = glm::cross(cb, ab);
	// va.normal += n1;
	// vb.normal += n1;
	// vc.normal += n1;

	const glm::vec3 ac = va.position - vc.position;
	const glm::vec3 dc = vd.position - vc.position;
	const glm::vec3 n2 = glm::cross(dc, ac);
	// va.normal += n2;
	// vc.normal += n2;
	// vd.normal += n2;

	mesh->indices.push_back(ia);
	mesh->indices.push_back(ib);
	mesh->indices.push_back(ic);

	mesh->indices.push_back(ia);
	mesh->indices.push_back(ic);
	mesh->indices.push_back(id);
}

Mesh *SnMeshExtractor::extract(Chunk *chunk)
{
    std::vector<int> inds(CHUNK_SIZE*CHUNK_SIZE*2*10);
    Mesh *result = new Mesh();
    for (int z = 0; z < CHUNK_SIZE; z++) {
        for (int y = 0; y < CHUNK_SIZE; y++) {
            for (int x = 0; x < CHUNK_SIZE; x++) {
                int cx = chunk->getBlock(x, y, z);
                const glm::vec3 p(x, y, z);
                const float vs[8] = {
                    chunk->getBlock(x,   y,   z),
                    chunk->getBlock(x+1, y,   z),
                    chunk->getBlock(x,   y+1, z),
                    chunk->getBlock(x+1, y+1, z),
                    chunk->getBlock(x,   y,   z+1),
                    chunk->getBlock(x+1, y,   z+1),
                    chunk->getBlock(x,   y+1, z+1),
                    chunk->getBlock(x+1, y+1, z+1)
                };

                const int config_n =
                    ((vs[0] < 0.0f) << 0) |
                    ((vs[1] < 0.0f) << 1) |
                    ((vs[2] < 0.0f) << 2) |
                    ((vs[3] < 0.0f) << 3) |
                    ((vs[4] < 0.0f) << 4) |
                    ((vs[5] < 0.0f) << 5) |
                    ((vs[6] < 0.0f) << 6) |
                    ((vs[7] < 0.0f) << 7);

                if (config_n == 0 || config_n == 255)
                    continue;

                glm::vec3 average(0);
                int average_n = 0;
                auto do_edge = [&](float va, float vb, int axis, const glm::vec3 &p) {
                    if ((va < 0.0) == (vb < 0.0))
                        return;

                    glm::vec3 v = p;
                    v[axis] += va / (va - vb);
                    average += v;
                    average_n++;
                };

                do_edge(vs[0], vs[1], 0, glm::vec3(x, y,     z));
                do_edge(vs[2], vs[3], 0, glm::vec3(x, y+1,   z));
                do_edge(vs[4], vs[5], 0, glm::vec3(x, y,     z+1));
                do_edge(vs[6], vs[7], 0, glm::vec3(x, y+1,   z+1));
                do_edge(vs[0], vs[2], 1, glm::vec3(x,   y,   z));
                do_edge(vs[1], vs[3], 1, glm::vec3(x+1, y,   z));
                do_edge(vs[4], vs[6], 1, glm::vec3(x,   y,   z+1));
                do_edge(vs[5], vs[7], 1, glm::vec3(x+1, y,   z+1));
                do_edge(vs[0], vs[4], 2, glm::vec3(x,   y,   z));
                do_edge(vs[1], vs[5], 2, glm::vec3(x+1, y,   z));
                do_edge(vs[2], vs[6], 2, glm::vec3(x,   y+1, z));
                do_edge(vs[3], vs[7], 2, glm::vec3(x+1, y+1, z));

                const glm::vec3 v = average / glm::vec3(average_n);                
                inds[offset_3d_slab(p, glm::vec3(CHUNK_SIZE))] = result->vertices.size();

                if (cx == -1) {
                    //cx = 6;
                    cx = chunk->getBlock(x, y, z+1);
                    if (cx == -1) {
                        cx = chunk->getBlock(x, y, z-1);
                        if (cx == -1) {
                            cx = chunk->getBlock(x+1, y, z); 
                            if (cx == -1) {
                                cx = chunk->getBlock(x-1, y, z);
                            }
                        }
                    }
                }
                result->vertices.push_back({v, cx});

                const bool flip = vs[0] < 0.0f;
                if (p.y > 0 && p.z > 0 && (vs[0] < 0.0f) != (vs[1] < 0.0f)) {
                    quad(result, chunk, flip,
                         inds[offset_3d_slab(glm::vec3(p.x, p.y,   p.z), glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x, p.y,   p.z-1), glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x, p.y-1, p.z-1), glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x, p.y-1, p.z),   glm::vec3(CHUNK_SIZE))]
                         );
                }
                if (p.x > 0 && p.z > 0 && (vs[0] < 0.0f) != (vs[2] < 0.0f)) {
                    quad(result, chunk, flip,
                         inds[offset_3d_slab(glm::vec3(p.x,   p.y, p.z),   glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x-1, p.y, p.z),   glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x-1, p.y, p.z-1), glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x,   p.y, p.z-1), glm::vec3(CHUNK_SIZE))]
                         );
                }
                if (p.x > 0 && p.y > 0 && (vs[0] < 0.0f) != (vs[4] < 0.0f)) {
                    quad(result, chunk, flip,
                         inds[offset_3d_slab(glm::vec3(p.x,   p.y,   p.z), glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x,   p.y-1, p.z), glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x-1, p.y-1, p.z), glm::vec3(CHUNK_SIZE))],
                         inds[offset_3d_slab(glm::vec3(p.x-1, p.y,   p.z), glm::vec3(CHUNK_SIZE))]
                         );
                }
            }
        }
    }
    return result;
}
