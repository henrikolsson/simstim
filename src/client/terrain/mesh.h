#ifndef _MESH_H
#define _MESH_H

#include <vector>
#include "../types.h"

#pragma pack(push, 0)

struct Vertex {
    glm::vec3 position;
    int material;
};

struct Mesh {
    std::vector<unsigned int> indices;
    std::vector<Vertex> vertices;
};

#pragma pack(pop)

#endif
