#ifndef _MC_MESH_EXTRACTOR_H
#define _MC_MESH_EXTRACTOR_H

#include "mesh_extractor.h"
#include "mesh.h"

class McMeshExtractor : MeshExtractor
{
  public:
    ~McMeshExtractor();
    Mesh *extract(Chunk *chunk);
};

#endif
