#ifndef _CUBIC_MESH_EXTRACTOR_H
#define _CUBIC_MESH_EXTRACTOR_H

#include "mesh_extractor.h"

class CubicMeshExtractor : MeshExtractor
{
  public:
    ~CubicMeshExtractor();
    Mesh *extract(Chunk *chunk);
};

#endif
