#include "cubic_mesh_extractor.h"
#include "../chunk.h"

CubicMeshExtractor::~CubicMeshExtractor()
{
}

Mesh *CubicMeshExtractor::extract(Chunk *chunk)
{
    Mesh *result = new Mesh();
    int blocks = 0;
    for (int x=0;x<CHUNK_SIZE;x++)
    {
        for (int y=0;y<CHUNK_SIZE;y++)
        {
            for (int z=0;z<CHUNK_SIZE;z++)
            {
                int c = chunk->getBlock(x, y, z);
                if (c <= 0)
                    continue;
                
                blocks++;
                
                //c = rand() % 5+1;
                
                // front
                Vertex v0 = {glm::vec3(x, y, z), c};
                Vertex v1 = {glm::vec3(x+1, y, z), c};
                Vertex v2 = {glm::vec3(x, y+1, z), c};
                Vertex v3 = {glm::vec3(x+1, y+1, z), c};

                // back
                Vertex v4 = {glm::vec3(x, y, z + 1), c};
                Vertex v5 = {glm::vec3(x+1, y, z + 1), c};
                Vertex v6 = {glm::vec3(x, y+1, z + 1), c};
                Vertex v7 = {glm::vec3(x+1, y+1, z + 1), c};
                
                //LOG(INFO) << "z: " <<  (z-1);
                if ((chunk->isAir(x,y,z-1) || (chunk->isTransparent(x,y,z-1) && !chunk->isTransparent(x,y,z))))
                {
                    //front
                    result->vertices.push_back(v0);
                    result->vertices.push_back(v1);
                    result->vertices.push_back(v3);
                    
                    result->vertices.push_back(v0);
                    result->vertices.push_back(v3);
                    result->vertices.push_back(v2);
                }
                
                if ((chunk->isAir(x,y,z+1) || (chunk->isTransparent(x,y,z+1) && !chunk->isTransparent(x,y,z))))
                {
                    //backs
                    result->vertices.push_back(v5);
                    result->vertices.push_back(v4);
                    result->vertices.push_back(v6);
                    
                    result->vertices.push_back(v5);
                    result->vertices.push_back(v6);
                    result->vertices.push_back(v7);
                }
                
                if ((chunk->isAir(x,y-1,z) || (chunk->isTransparent(x,y-1,z) && !chunk->isTransparent(x,y,z))))
                {
                    //bottom
                    result->vertices.push_back({glm::vec3(x, y, z + 1), c-16});
                    result->vertices.push_back({glm::vec3(x+1, y, z + 1), c-16});
                    result->vertices.push_back({glm::vec3(x+1, y, z), c-16});
                    
                    result->vertices.push_back({glm::vec3(x, y, z + 1), c-16});
                    result->vertices.push_back({glm::vec3(x+1, y, z), c-16});
                    result->vertices.push_back({glm::vec3(x, y, z), c-16});
                }

                if ((chunk->isAir(x,y+1,z) || (chunk->isTransparent(x,y+1,z) && !chunk->isTransparent(x,y,z))))
                {
                    //top
                    result->vertices.push_back({glm::vec3(x, y+1, z), c-16});
                    result->vertices.push_back({glm::vec3(x+1, y+1, z), c-16});
                    result->vertices.push_back({glm::vec3(x+1, y+1, z + 1), c-16});
                    
                    result->vertices.push_back({glm::vec3(x, y+1, z), c-16});
                    result->vertices.push_back({glm::vec3(x+1, y+1, z + 1), c-16});
                    result->vertices.push_back({glm::vec3(x, y+1, z + 1), c-16});
                }

                if ((chunk->isAir(x-1,y,z) || (chunk->isTransparent(x-1,y,z) && !chunk->isTransparent(x,y,z))))
                {
                    //side left
                    result->vertices.push_back(v4);
                    result->vertices.push_back(v0);
                    result->vertices.push_back(v2);
                    
                    result->vertices.push_back(v4);
                    result->vertices.push_back(v2);
                    result->vertices.push_back(v6);
                }
                
                if ((chunk->isAir(x+1,y,z) || (chunk->isTransparent(x+1,y,z) && !chunk->isTransparent(x,y,z))))
                {
                    //side right
                    result->vertices.push_back(v1);
                    result->vertices.push_back(v5);
                    result->vertices.push_back(v7);
                    
                    result->vertices.push_back(v1);
                    result->vertices.push_back(v7);
                    result->vertices.push_back(v3);
                }
            }
        }
    }
    return result;
}
