#include "text.h"

Text::Text()
{
    program.attachShader("text.v.glsl", GL_VERTEX_SHADER);
    program.attachShader("text.f.glsl", GL_FRAGMENT_SHADER);
    program.link();
    
    program.use();
    attribute_coord = program.getAttrib("vert");
    uniform_tex = program.getUniform("tex");
    uniform_color = program.getUniform("color");
    glGenBuffers(1, &vbo);
    if(FT_Init_FreeType(&ft)) {
        throw std::runtime_error("Could not init freetype library");
    }
    
    if(FT_New_Face(ft, "../assets/fonts/DejaVuSansMono.ttf", 0, &face)) {
        throw std::runtime_error("Could not open font DejaVuSansMono.ttf");
    }
    FT_Set_Pixel_Sizes(face, 0, 24);
    if(FT_Load_Char(face, 'X', FT_LOAD_RENDER)) {
        throw std::runtime_error("Could not load character from font");
    }
    g = face->glyph;
  
}

Text::~Text()
{
    
}

void Text::addLine(std::string line)
{
    lines.push_back(line);
}

void Text::render()
{
    program.use();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    CHECK_GL_ERRORS();
        
    glGenTextures(1, &tex);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex);
    glUniform1i(uniform_tex, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glEnableVertexAttribArray(static_cast<GLuint>(attribute_coord));
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(static_cast<GLuint>(attribute_coord), 4, GL_FLOAT, GL_FALSE, 0, 0);
    CHECK_GL_ERRORS();
 
    GLfloat black[4] = {1, 1, 1, 1};
    glUniform4fv(uniform_color, 1, black);
    // TODO: No hardcoding of resolution
    float sx = 2.0 / 1024;
    float sy = 2.0 / 768;

    for (uint i=0;i<lines.size();i++)
    {
        float x = -1 + 5 * sx;
        float y = 1 - (24*(i+1)) * sy;
        CHECK_GL_ERRORS();
        std::string str = lines[i];
        for(std::string::iterator it = str.begin(); it != str.end(); ++it) {

            if(FT_Load_Char(face, static_cast<FT_ULong>(*it), FT_LOAD_RENDER))
            {
                LOG(ERROR) << "Failed to load character: " << *it;
                continue;
            }
            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_RED,
                         g->bitmap.width,
                         g->bitmap.rows,
                         0,
                         GL_RED,
                         GL_UNSIGNED_BYTE,
                         g->bitmap.buffer);

            CHECK_GL_ERRORS();
 
            float x2 = x + g->bitmap_left * sx;
            float y2 = -y - g->bitmap_top * sy;
            float w = g->bitmap.width * sx;
            float h = g->bitmap.rows * sy;
 
            GLfloat box[4][4] = {
                {x2,     -y2    , 0, 0},
                {x2 + w, -y2    , 1, 0},
                {x2,     -y2 - h, 0, 1},
                {x2 + w, -y2 - h, 1, 1},
            };
 
            glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
            CHECK_GL_ERRORS();
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
            CHECK_GL_ERRORS();
 
            x += (g->advance.x >> 6) * sx;
            y += (g->advance.y >> 6) * sy;
        }
    }
    lines.clear();
    glDisableVertexAttribArray(static_cast<GLuint>(attribute_coord));
    glDeleteTextures(1, &tex);
    glUseProgram(0);
}
