#include "chunk.h"
#include "world.h"
#include "terrain/mesh_extractor.h"
#include "terrain/mc_mesh_extractor.h"
#include "terrain/cubic_mesh_extractor.h"
#include "terrain/sn_mesh_extractor.h"
#include "textures.c"

Chunk::Chunk(int32_t cx, int32_t cy, int32_t cz, World *world)
{
    m_cx = cx;
    m_cy = cy;
    m_cz = cz;
    m_world = world;
}
Chunk::~Chunk()
{
    if (mesh != nullptr)
        delete mesh;
}

int32_t Chunk::getChunkX()
{
    return m_cx;
}

int32_t Chunk::getChunkY()
{
    return m_cy;
}

int32_t Chunk::getChunkZ()
{
    return m_cz;
}

int32_t Chunk::getWorldX()
{
    return m_cx * CHUNK_SIZE;
}

int32_t Chunk::getWorldY()
{
    return m_cy * CHUNK_SIZE;
}

int32_t Chunk::getWorldZ()
{
    return m_cz * CHUNK_SIZE;
}

void Chunk::setData(const std::string &data)
{
#ifdef MOCK_VOXELS
    for (int x=0;x<CHUNK_SIZE;x++)
    {
        for (int y=0;y<CHUNK_SIZE;y++)
        {
            for (int z=0;z<CHUNK_SIZE;z++)
            {
                m_data[CHUNK_IDX(x,y,z)] = -1;
            }
        }
    }

    // m_data[CHUNK_IDX(6,5,6)] = 8;
    // m_data[CHUNK_IDX(5,6,6)] = 8;
    // m_data[CHUNK_IDX(6,6,6)] = 8;
    // m_data[CHUNK_IDX(7,6,6)] = 8;
    m_data[CHUNK_IDX(6,7,6)] = 8;
    m_data[CHUNK_IDX(6,7,7)] = 8;
    m_data[CHUNK_IDX(6,7,8)] = 8;
#else
    memcpy(&m_data, data.c_str(), sizeof(char) * CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);
#endif
}

uint64_t Chunk::getLastUsed()
{
    return m_lastUsed;
}

#define voidp_offsetof(type, member)                    \
    reinterpret_cast<void*>(offsetof(type, member))

void Chunk::initialize(Program *program)
{
    glGenBuffers(1, &m_vbo);

    std::vector<MeshExtractor *> shape;
    currentME = 0;
    if (currentME == 0) {
        CubicMeshExtractor mce;
        mesh = mce.extract(this);
    }
    else if (currentME == 1) {
        McMeshExtractor mce;
        mesh = mce.extract(this);
    }
    else if (currentME == 2) {
        SnMeshExtractor mce;
        mesh = mce.extract(this);
    }
    else {
        throw std::runtime_error("Invalid me");
    }
    m_elements = mesh->vertices.size();
    if (m_elements == 0)
        return;
        
    CHECK_GL_ERRORS();
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<GLsizeiptr>(mesh->vertices.size() * sizeof(Vertex)),
                 &mesh->vertices[0],
                 GL_STATIC_DRAW);
    
    GLuint attribute_coord = static_cast<GLuint>(program->getAttrib("vert"));
    glEnableVertexAttribArray(attribute_coord);
    glVertexAttribPointer(attribute_coord, 3, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex), voidp_offsetof(Vertex, position));
    CHECK_GL_ERRORS();

    attribute_coord = static_cast<GLuint>(program->getAttrib("material"));
    glEnableVertexAttribArray(attribute_coord);
    glVertexAttribIPointer(attribute_coord, 1, GL_INT,
                          sizeof(Vertex), voidp_offsetof(Vertex, material));
    CHECK_GL_ERRORS();

    m_initialized = true;

    if (mesh->indices.size() > 0)
    {
        glGenBuffers(2, &m_idx);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_idx);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     static_cast<GLsizeiptr>(mesh->indices.size() * sizeof(unsigned int)),
                     &mesh->indices[0],
                     GL_STATIC_DRAW);
        CHECK_GL_ERRORS();
    }

    
    GLuint cursor_vbo;
    glGenBuffers(1, &cursor_vbo);

    // GLuint texture;
    // GLint uniform_texture;
    // glActiveTexture(GL_TEXTURE0);
    // glGenTextures(1,&texture);
    // glBindTexture(GL_TEXTURE_2D_ARRAY,texture);
    // //Allocate the storage.
    // glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, 16, 16, 1);
    // //Upload pixel data.
    // //The first 0 refers to the mipmap level (level 0, since there's only 1)
    // //The following 2 zeroes refers to the x and y offsets in case you only want to specify a subrectangle.
    // //The final 0 refers to the layer index offset (we start from index 0 and have 2 levels).
    // //Altogether you can specify a 3D box subset of the overall texture, but only one mip level at a time.
    // glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, 16, 16, 1, GL_RGBA, GL_UNSIGNED_BYTE, textures.pixel_data);
    // uniform_texture = program->getUniform("tex");
 
    // //Always set reasonable texture parameters
    // glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
    // CHECK_GL_ERRORS();
    GLuint texture;
    GLint uniform_texture;
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 static_cast<GLsizei>(textures.width),
                 static_cast<GLsizei>(textures.height),
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 textures.pixel_data);
    //glGenerateMipmap(GL_TEXTURE_2D);
    uniform_texture = program->getUniform("texture");
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void Chunk::toggleMeshExtractor()
{
    currentME++;
    if (currentME == 3)
        currentME = 0;
    
    LOG(INFO) << currentME;
    
    m_initialized = false;
}

void Chunk::render(Program *program)
{
    if (!m_initialized) {
        this->initialize(program);
    }
    
    m_lastUsed = get_nanos();
    CHECK_GL_ERRORS();

    m_lastUsed = get_nanos();
    CHECK_GL_ERRORS();
    GLuint attribute_coord = static_cast<GLuint>(program->getAttrib("vert"));
    glEnableVertexAttribArray(attribute_coord);
    GLuint materialx = static_cast<GLuint>(program->getAttrib("material"));
    glEnableVertexAttribArray(materialx);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    
    glVertexAttribPointer(attribute_coord, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          voidp_offsetof(Vertex, position));
    glVertexAttribIPointer(materialx, 1, GL_INT, sizeof(Vertex),
                          voidp_offsetof(Vertex, material));
    
    GLint uniform_texture = program->getUniform("texture");
    glUniform1i(uniform_texture, 0);

    if (mesh->indices.size() > 0)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_idx);
        glDrawElements(GL_TRIANGLES,
                       mesh->indices.size(),
                       GL_UNSIGNED_INT,
                       static_cast<void*>(0));
        CHECK_GL_ERRORS();
    }
    else
    {
        glDrawArrays(GL_TRIANGLES, 0, mesh->vertices.size());
        CHECK_GL_ERRORS();
    }
    //glDrawArrays(GL_LINES, 0, m_elements);
    //glDisableVertexAttribArray(attribute_coord);
    CHECK_GL_ERRORS();
}

int Chunk::numberOfVertices()
{
    if (mesh != nullptr)
        return mesh->vertices.size();
    else
        return 0;
}

/*
 15


 */
float Chunk::getBlock(int32_t x, int32_t y, int32_t z)
{
    if (x < CHUNK_SIZE &&
        y < CHUNK_SIZE &&
        z < CHUNK_SIZE &&
        x >= 0 &&
        y >= 0 &&
        z >= 0)
    {
        return m_data[CHUNK_IDX(x,y,z)];
    }
    else
    {
        int ax = m_cx * CHUNK_SIZE + x;
        int ay = m_cy * CHUNK_SIZE + y;
        int az = m_cz * CHUNK_SIZE + z;
        
        float r = m_world->getBlock(ax, ay, az);
        //LOG(INFO) << "Getting outside: " << ax << ", " << ay << ", " << az;
        return r;
    }
}

bool Chunk::isAir(int32_t x, int32_t y, int32_t z)
{
    float block = this->getBlock(x, y, z);
    return block <= 0;
}

bool Chunk::isTransparent(int32_t x, int32_t y, int32_t z)
{
    float block = this->getBlock(x, y, z);
    return block <= 0 || block == 8;
}
