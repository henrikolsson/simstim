#ifndef _ENTITY_H
#define _ENTITY_H

#include <string>
#include <glm/glm.hpp>
#include <cstdint>
#include "program.h"

using std::int32_t;

class Entity
{
 public:
    Entity(int32_t id, Program *program);
    ~Entity();
    void render();
    int32_t getId();
    glm::vec3 getPosition();
    void setPosition(glm::vec3 pos);
 private:
    int32_t m_id;
    Program *m_program;
    GLuint m_vao;
    glm::vec3 m_position;
};

#endif
