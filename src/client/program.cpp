#include "program.h"
 
Program::Program()
{
    m_program = glCreateProgram();
    CHECK_GL_ERRORS();
}

Program::~Program()
{
    glDeleteProgram(m_program);
    for (GLuint shader : m_shaders)
    {
        glDeleteShader(shader);
    }
}

void Program::attachShader(std::string filename, GLenum type)
{
    LOG(INFO) << "Attaching shader: " << filename;
    std::ifstream in(SHADER_PATH + filename);
    if (!in.is_open())
        throw std::runtime_error("unable to open file: " + SHADER_PATH + filename);
    
    std::string content((std::istreambuf_iterator<char>(in)),
                        std::istreambuf_iterator<char>());
    
    GLuint res = glCreateShader(type);
    const GLchar* sources[] = {content.c_str()};
    glShaderSource(res, 1, sources, NULL);
    glCompileShader(res);
    GLint compile_ok = GL_FALSE;
    glGetShaderiv(res, GL_COMPILE_STATUS, &compile_ok);
    if (compile_ok == GL_FALSE)
    {
        GLint log_length;
        glGetShaderiv(res, GL_INFO_LOG_LENGTH, &log_length);
        
        char *log = new char[log_length];
        glGetShaderInfoLog(res, log_length, NULL, log);
        
        std::string err = std::string("error compiling shader: ") +
            filename +
            ": \n" +
            log;
        throw std::runtime_error(err);
    }

    glAttachShader(m_program, res);
    m_shaders.push_back(res);
}

void Program::link()
{
    glLinkProgram(m_program);
    GLint link_ok = GL_FALSE;
    glGetProgramiv(m_program, GL_LINK_STATUS, &link_ok);
    if (!link_ok)
    {
        GLint log_length;
        glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &log_length);
        
        char *log = new char[log_length];
        glGetProgramInfoLog(m_program, log_length, NULL, log);
        
        std::string err = std::string("error linking program: \n") + log;
        throw std::runtime_error(err);
    }
}

void Program::use()
{
    glUseProgram(m_program);
}

GLint Program::getAttrib(std::string name)
{
    GLint ret = glGetAttribLocation(m_program, name.c_str());
    if (ret == -1)
        throw std::runtime_error("no such attribute: " + name);
    CHECK_GL_ERRORS();
    return ret;
}

GLint Program::getUniform(std::string name)
{
    GLint ret = glGetUniformLocation(m_program, name.c_str());
    if (ret == -1)
        throw std::runtime_error("no such uniform: " + name);
    CHECK_GL_ERRORS();
    return ret;
}
