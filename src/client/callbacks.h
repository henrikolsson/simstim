#ifndef _CALLBACKS_H
#define _CALLBACKS_H

void glfwCursorPos(GLFWwindow *window, double x, double y);
void glfwKey(GLFWwindow *window, int key, int scancode, int action, int mods);
void glfwSetWindowSize2(GLFWwindow *window, int width, int height);
void glfwError(int code, const char *error);
void glfwWindowFocus(GLFWwindow *window, int focused);
void glfwMouseButton(GLFWwindow *window, int button, int action, int mods);

#endif
