#ifndef _WORLD_H
#define _WORLD_H

#include <cstdint>
#include <vector>
#include "chunk.h"
#include "../common/config.h"

#define W_INDEX_TO_C_INDEX(p) (floor(static_cast<float>(p) / CHUNK_SIZE))
#define DEFAULT_BLOCK_VALUE -1.0

class World
{
 public:
    World();
    Chunk *getChunkAt(int32_t cx, int32_t cy, int32_t cz);
    void setChunkAt(int32_t cx, int32_t cy, int32_t cz, const std::string &data);
    std::vector<Chunk *> getChunks();
    void toggleMeshExtractor();
    int loadedChunks();
    float getBlock(int ax, int ay, int az);
 private:
    std::vector<Chunk *> m_chunks;
    bool m_marchingCubes = true;
};

#endif

/*
  -1, -1, -1
  16 
 */
