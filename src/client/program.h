#ifndef _PROGRAM_H
#define _PROGRAM_H

#include <string>
#include <fstream>
#include <vector>
#include <GL/glew.h>
#include "gutil.h"

#define SHADER_PATH std::string("../shaders/")

class Program
{
 public:
    Program();
    ~Program();
    void attachShader(std::string filename, GLenum type);
    void link();
    void use();
    GLint getAttrib(std::string name);
    GLint getUniform(std::string name);
 private:
    GLuint m_program;
    std::vector<GLuint> m_shaders;
};

#endif
