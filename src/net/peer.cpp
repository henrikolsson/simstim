#include "peer.h"

Peer::Peer()
{
}

Peer::~Peer()
{
}

void Peer::send(ENetPeer *peer, uint32_t packetType, std::string &pkt)
{
    char *data = new char[pkt.size() + sizeof(uint32_t) * 2];
    uint32_t tmp;

    tmp = packetType;
    memcpy(data, &tmp, sizeof(uint32_t));
    
    tmp = pkt.size();
    memcpy(data + sizeof(uint32_t), &tmp, sizeof(uint32_t));
    
    memcpy(data + sizeof(uint32_t) * 2, pkt.c_str(), pkt.size());
    ENetPacket * packet = enet_packet_create(data,
                                             pkt.size() + sizeof(uint32_t) * 2,
                                             ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send(peer, 0, packet);
    delete[] data;
}

void Peer::recv(ENetEvent &event)
{
    uint8_t *data = event.packet->data;
    uint dataLength = event.packet->dataLength;
    if (dataLength < static_cast<int32_t>(sizeof(uint32_t) * 2))
    {
        LOG(ERROR) << "Invalid packet length: " << dataLength;
        return;
    }
    uint consumed = 0;
    while (consumed < dataLength)
    {
        uint32_t packetType;
        uint32_t packetLength;
        memcpy(&packetType, data + consumed, sizeof(uint32_t));
        memcpy(&packetLength, data + consumed + sizeof(uint32_t), sizeof(uint32_t));
        char *buf = new char[packetLength];
        memcpy(buf, data + consumed + sizeof(uint32_t) * 2, packetLength);
        std::string d(buf, packetLength);
        this->handlePacket(event.peer, packetType, d);
        consumed += sizeof(uint32_t) + sizeof(uint32_t) + packetLength;
        delete[] buf;
    }
}

char *Peer::hostToStr(enet_uint32 addr)
{
    char *res = new char[20];
    snprintf(res, 19, "%d.%d.%d.%d",
             (addr & 0xFF),
             (addr >> 8 & 0xFF),
             (addr >> 16 & 0xFF),
             (addr >> 24 & 0xFF));
    return res;
}
