#define STATE_CONNECTED 1
#define STATE_HELLO_RECEIVED 2
#define STATE_REGISTERED 3

typedef struct connection {
    glm::vec3 position;
    glm::vec3 right;
    glm::vec3 forward;
    int state;
    std::string name;
    ENetPeer *peer;
    int move;
} connection;
