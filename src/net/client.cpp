#include "client.h"

#define CLIENT_STEP (1000.0f/3.0f)

Client::Client()
{
    m_client = nullptr;
    m_peer = nullptr;
}

bool Client::connect(const char* host, int port)
{
    ENetAddress address;
    ENetEvent event;
    
    LOG(INFO) << "Connecting to: " << host << ":" << port;
    enet_address_set_host(&address, host);
    address.port = port;
    m_client = enet_host_create(NULL, 32, 2, 0, 0);
    m_peer = enet_host_connect(m_client, &address, 32, 0);
    if (m_peer == NULL)
    {
        LOG(ERROR) << "No available peers for initiating an ENet connection.";
        return false;
    }
    
    if (enet_host_service (m_client, & event, 5000) > 0 &&
        event.type == ENET_EVENT_TYPE_CONNECT)
    {
        return true;
    }
    else
    {
        enet_peer_reset(m_peer);
        return false;
    }
}

Client::~Client()
{
    this->disconnect();
}

void Client::disconnect()
{
    if (m_peer != nullptr)
    {
        LOG(INFO) << "Disconnecting";
        enet_peer_disconnect(m_peer, 0);
        this->work();
    }
    if (m_client != nullptr)
    {
        enet_host_destroy(m_client);
    }
    m_client = nullptr;
    m_peer = nullptr;
}

void Client::hello()
{
    Hello req;
    req.set_name("HI");
    NET_SEND(PROTO_HELLO,req);
}

uint64_t Client::getPacketCounter()
{
    return m_packets;
}

void Client::work()
{
    float time_now = get_millis();
    float time_delta = time_now - m_time_last;
    if (time_delta >= CLIENT_STEP)
    {
        Move req;
        req.set_direction(m_input);
        req.set_allocated_forward(glmToProto(m_forward));
        req.set_allocated_right(glmToProto(m_right));
        // req.set_forwardx(m_camera->getForwardDirection().x);
        // req.set_forwardy(m_camera->getForwardDirection().y);
        // req.set_forwardz(m_camera->getForwardDirection().z);
        // req.set_rightx(m_camera->getRightDirection().x);
        // req.set_righty(m_camera->getRightDirection().y);
        // req.set_rightz(m_camera->getRightDirection().z);
        NET_SEND(PROTO_MOVE, req);
        m_packets++;
        m_time_last = get_millis();
    }
    time_delta = time_now - m_time_last_second;
    if (time_delta >= 5000)
    {
        LOG(INFO) << "pps: " << m_packets / (get_millis() - m_time_last_second) * 1000;
        m_packets = 0;
        m_time_last_second = time_now;
    }
    
    for (auto c : m_chunkQueue)
    {
        GetChunk gc = *c;
        NET_SEND(PROTO_GET_CHUNK, gc);
        m_chunksPending.push_back(c);
    }
    m_chunkQueue.clear();

    ENetEvent event;
    while (enet_host_service(m_client, &event, 0) > 0)
    {
        switch (event.type)
        {
            case ENET_EVENT_TYPE_CONNECT:
                LOG(INFO) << "Connected";
                break;
                    
            case ENET_EVENT_TYPE_RECEIVE:
                this->recv(event);
                this->packets.push(*event.packet);
                break;
                    
            case ENET_EVENT_TYPE_DISCONNECT:
                LOG(INFO) << "Disconnect";
                break;
                    
            case ENET_EVENT_TYPE_NONE:
                break;

            default:
                LOG(ERROR) << "Unhandled enet type";
        }
    }
    LOG(INFO) << "WORK DONE";
}

void Client::handlePacket(ENetPeer *peer, uint32_t packetType, std::string &pkt)
{
    if (packetType == PROTO_SET_POSITION)
    {
        this->handleSetPosition(pkt);
    }
    else if (packetType == PROTO_CHUNK_DATA)
    {
        this->handleChunkData(pkt);
    }
    else
    {
        LOG(ERROR) << "Unknown packet type: " << packetType;
    }
}

void Client::handleSetPosition(std::string& buf)
{
    SetPosition pos;
    pos.ParseFromString(buf);
    if (m_callbacks != nullptr)
        m_callbacks->clientSetPosition(&pos);
}

void Client::handleChunkData(std::string& buf)
{
    ChunkData cd;
    cd.ParseFromString(buf);
    if (m_callbacks != nullptr)
        m_callbacks->clientChunkData(&cd);
    
    for(std::vector<GetChunk *>::iterator it = m_chunksPending.begin();
        it != m_chunksPending.end();
        ++it)
    {
        GetChunk *gc = *it;
        if (gc->cx() == cd.cx() &&
            gc->cy() == cd.cy() &&
            gc->cz() == cd.cz())
        {
            LOG(INFO) << "Got pending chunk";
            m_chunksPending.erase(it);
            return;
        }
    }
    LOG(INFO) << "This should never happen!!";
}

void Client::setCallbacks(ClientCallbacks *cc)
{
    m_callbacks = cc;
}

void Client::setInput(int input)
{
    this->m_input = input;
}

void Client::setForward(glm::vec3 forward)
{
    this->m_forward = forward;
}

void Client::setRight(glm::vec3 right)
{
    this->m_right = right;
}

void Client::getChunk(int32_t cx, int32_t cy, int32_t cz)
{
    for (auto c : m_chunkQueue)
    {
        if (c->cx() == cx && c->cy() == cy && c->cz() == cz)
            return;
    }
    for (auto c : m_chunksPending)
    {
        if (c->cx() == cx && c->cy() == cy && c->cz() == cz)
            return;
    }

    LOG(INFO) << "Getting chunk: " << cx << ", " << cy << ", " << cz;

    GetChunk *gc = new GetChunk();
    gc->set_cx(cx);
    gc->set_cy(cy);
    gc->set_cz(cz);
    m_chunkQueue.push_back(gc);
}
