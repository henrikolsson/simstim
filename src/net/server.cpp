#include "server.h"
#include "../common/noise.h"

Server::Server(const char* host, const int port)
{
    m_noise = new OpenSimplexNoise();
    enet_address_set_host(&m_address, host);
    m_address.port = port;
    m_server = enet_host_create(&m_address, 32, 2, 0, 0);
    if (m_server == NULL)
    {
        throw std::runtime_error("failed to create server");
    }
    LOG(INFO) << "Server listening on port " << m_server->address.port;
}

Server::~Server()
{
    this->stop();
    enet_host_destroy(m_server);
    delete m_noise;
    LOG(INFO) << "Server destroyed";
}

void Server::runAsync()
{
    LOG(INFO) << "Running async";
    m_thread = std::thread([&] { this->run(); });
}

void Server::run()
{
    float time_now = get_millis();
    float time_old = time_now;
    float time_delta = 0;
    float time_acc = 0;
    float time_last = 0;
    float time_start = get_millis();
    int packets = 0;
    uint64_t sleep_time = 0;
    int tick_counter = 0;
    LOG(INFO) << "step: " << STEP_SIZE;
    while (!m_stop)
    {
        if (get_millis() - time_last > 1000)
        {
            LOG(INFO) << "broadcasts per second: " << (packets / (get_millis()-time_start) * 1000);
            LOG(INFO) << "ticks per second: " << (tick_counter / (get_millis()-time_start) * 1000);
            LOG(INFO) << "sleep: " << sleep_time << "/" <<  (get_millis()-time_start);
            time_last = get_millis();
        }
        
        time_now = get_millis();
        time_delta = time_now - time_old;
        if (time_delta < 0)
        {
            LOG(INFO) << "delta: " << time_delta;
            LOG(INFO) << "now: " << time_now;
            LOG(INFO) << "old: " << time_old;
            throw std::runtime_error("Time went backwards!");
        }
        time_old = time_now;
        

        if (time_delta > 0.5f)
            time_delta = 0.5f;

        time_acc += time_delta;

        while (time_acc >= STEP_SIZE)
        {
            tick_counter += 1;

            // recv_packets(tick_counter);
            // step_simulation_forward(tick_counter);
            

            for (auto &c : clients)
            {
                const float movespeed = 0.008;
                //LOG(INFO) << " before: " << c.position.x << " , "  << c.position.y << " , "  << c.position.z;
                int i = 0;
                if (c.move & PROTO_MOVE_DIRECTION_BACKWARD)
                {
                    c.position -= c.forward * movespeed * STEP_SIZE;
                }
                if (c.move & PROTO_MOVE_DIRECTION_FORWARD)
                {
                    //LOG(INFO) << " c.forward: " << c.forward.x << " , "  << c.forward.y << " , "  << c.forward.z;
                    c.position += c.forward * movespeed * STEP_SIZE;
                }
                if (c.move & PROTO_MOVE_DIRECTION_LEFT)
                {
                    c.position -= c.right * movespeed * STEP_SIZE;
                }
                if (c.move & PROTO_MOVE_DIRECTION_RIGHT)
                {
                    c.position += c.right * movespeed * STEP_SIZE;
                }
                if (c.move & PROTO_MOVE_DIRECTION_UP)
                {
                    c.position.y += movespeed * STEP_SIZE;
                }
                if (c.move & PROTO_MOVE_DIRECTION_DOWN)
                {
                    c.position.y -= movespeed * STEP_SIZE;
                }
                i++;
                //LOG(INFO) << " after: " << c.position.x << " , "  << c.position.y << " , "  << c.position.z;
            }
            
            if ((tick_counter % SERVER_SEND_RATE) == 0)
            {
                for (auto c : clients)
                {
                    int i = 0;
                    if (c.state == STATE_HELLO_RECEIVED)
                    {
                        SetPosition res;
                        res.set_allocated_position(glmToProto(c.position));
                        res.set_entityid(-1);
                        //NET_SEND_PEER(c.peer, PROTO_SET_POSITION, res);
                    }
                    i++;
                }
                packets++;
            }
            //LOG(INFO) << "acc: " << time_acc << " step: " << STEP_SIZE;
            time_acc -= STEP_SIZE;
        }
        // render_frame(time_delta);

        ENetEvent event;
        while (enet_host_service(m_server, &event, 0) > 0)
        {
            switch (event.type)
            {
                case ENET_EVENT_TYPE_CONNECT:
                    this->connected(event);
                    break;
                    
                case ENET_EVENT_TYPE_RECEIVE:
                    this->recv(event);
                    break;
                    
                case ENET_EVENT_TYPE_DISCONNECT:
                    this->disconnected(event);
                    break;
                    
                case ENET_EVENT_TYPE_NONE:
                    break;
                    
                default:
                    LOG(ERROR) << "Unhandled enet type";
            }
        }


        //usleep(1);
        // Sleep remaining time
        float elapsed = get_millis() - time_now;
        if (elapsed < STEP_SIZE)
        {
            sleep_time += (unsigned int)((STEP_SIZE - elapsed));
            //usleep(sleep_time * 1000);
        }
    }
}

void Server::handlePacket(ENetPeer *peer, uint32_t packetType, std::string &pkt)
{
    int clientIdx = *((int*)peer->data);
    if (packetType == PROTO_HELLO)
    {
        LOG(INFO) << "PROTO_HELLO";
        this->handleHello(pkt, clients[clientIdx]);
    }
    else if (packetType == PROTO_MOVE)
    {
        this->handleMove(pkt, clients[clientIdx]);
    }
    else if (packetType == PROTO_GET_CHUNK)
    {
        this->handleGetChunk(pkt, clients[clientIdx]);
    }
    else
    {
        LOG(ERROR) << "Unknown packet type: " << packetType;
    }
}

int Server::getMaterial(int x, int y, int z)
{
    int width = 2 * 2 * CHUNK_SIZE;
    int v = 0;
    int intensity = 16;
    float xd = x-width*0.;
    float yd = y-width*0.20;
    float zd = z-width*0.25;
    if(yd > 0) yd *= yd*0.05;
    float xz = m_noise->eval(x, z, 0);
    float distance = (xd*xd+yd*yd*intensity+zd*zd)*0.0004;
    float density = m_noise->eval(x/intensity, y/intensity, z/intensity)-distance;
    if(density > -0.75){
        return 3;
    }
    if(density > -0.85){
        return 2;
    }
    if(density > -1.0){
        return y > intensity+xz*4 ? 1 : 2;
    }
    return 0;
}
static float noise3d_abs(float x, float y, float z, int seed, int octaves, float persistence) {
    float sum = 0;
    float strength = 1.0;
    float scale = 1.0;

    for(int i = 0; i < octaves; i++) {
        sum += strength * fabs(glm::simplex(glm::vec3(x, y, z) * scale));
        scale *= 2.0;
        strength *= persistence;
    }

    return sum;
}

void Server::handleGetChunk(std::string& buf, connection& con)
{
    ASSERT_STATUS(con, STATE_HELLO_RECEIVED);

    GetChunk gc;
    gc.ParseFromString(buf);

    ChunkData cd;
    cd.set_cx(gc.cx());
    cd.set_cy(gc.cy());
    cd.set_cz(gc.cz());
    char *data = new char[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
    for (uint32_t x=0;x<CHUNK_SIZE;x++)
    {
        for (uint32_t z=0;z<CHUNK_SIZE;z++)
        {
            uint32_t ax = gc.cx() * CHUNK_SIZE + x;
            uint32_t az = gc.cz() * CHUNK_SIZE + z;
            float n = m_noise->noise2d(ax / 128.0, az / 128.0, 123, 16, 0.2) * 4;
            uint32_t h = n * 8 + 10;
            for (uint32_t y=0;y<CHUNK_SIZE;y++)
            {
                data[CHUNK_IDX(x, y, z)] = -1;
                uint32_t ay = gc.cy() * CHUNK_SIZE + y;
                //float nn = m_noise->eval(ax / CHUNK_SIZE, ay / CHUNK_SIZE, az / CHUNK_SIZE);
                if (ay < h)
                {
                    if (y + ay * CHUNK_SIZE == 0)
                    {
                        data[CHUNK_IDX(x,y,z)] = 1;
                        continue;
                    }
                    
                    // Are we above "ground" level?
                    // if(y + ay * CHUNK_SIZE >= h)
                    // {
                    //     // If we are not yet up to sea level, fill with water blocks
                    //     if(y + ay * CHUNK_SIZE < 4)
                    //     {
                    //         data[CHUNK_IDX(x,y,z)] = 8;
                    //         continue;
                    //         // Otherwise, we are in the air
                    //     }
                    //     else
                    //     {
                    //         break;
                    //     }
                    // }
                
                    // Random value used to determine land type
                    float r = noise3d_abs((x + ax * CHUNK_SIZE) / 16.0,
                                          (y + ay * CHUNK_SIZE) / 16.0,
                                          (z + az * CHUNK_SIZE) / 16.0,
                                          -123121, 2, 1);
                
                    // Sand layer
                    if(n + r * 5 < 4)
                        data[CHUNK_IDX(x,y,z)] = 7;
                    // Dirt layer, but use grass blocks for the top
                    else if(n + r * 5 < 8)
                        data[CHUNK_IDX(x,y,z)] = (h < 4 || y + ay * CHUNK_SIZE < h - 1) ? 1 : 3;
                    // Rock layer
                    else if(r < 1.25)
                        data[CHUNK_IDX(x,y,z)] = 6;
                    // Sometimes, ores!
                    else
                        data[CHUNK_IDX(x,y,z)] = 3;
                    //data[CHUNK_IDX(x,y,z)] = 8;
                    
                }
                else
                {
                    if (ay == 0) {
                        data[CHUNK_IDX(x,y,z)] = 6;
                    }
                    else if (ay < 5) {
                        data[CHUNK_IDX(x,y,z)] = 8;
                    }
                    else {
                        data[CHUNK_IDX(x, y, z)] = -1;
                    }
                }
            }
        }
    }
    cd.set_data(data, CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * sizeof(char));
    NET_SEND_PEER(con.peer, PROTO_CHUNK_DATA, cd);
    delete[] data;
}

void Server::handleMove(std::string& buf, connection& con)
{
    ASSERT_STATUS(con, STATE_HELLO_RECEIVED);

    Move move;
    move.ParseFromString(buf);
    con.move = move.direction();
    con.forward = protoToGlm(&move.forward());
    con.right = protoToGlm(&move.right());
}

void Server::handleHello(std::string& buf, connection& con)
{
    ASSERT_STATUS(con, STATE_CONNECTED);

    Hello hello;
    hello.ParseFromString(buf);
    con.name = hello.name();
    con.state = STATE_HELLO_RECEIVED;
    con.move = PROTO_MOVE_DIRECTION_NONE;
    con.position = glm::vec3(4, 16, 3);
    
    SetPosition res;
    res.set_allocated_position(glmToProto(con.position));
    res.set_entityid(-1);
    NET_SEND_PEER(con.peer, PROTO_SET_POSITION, res);
}

void Server::connected(ENetEvent &event)
{
    char *host = hostToStr(event.peer->address.host);
    LOG(INFO) << "New client: " <<
        host <<
        ":" << event.peer->address.port;
    delete[] host;
    
    connection c;
    c.state = STATE_CONNECTED;
    c.peer = event.peer;
    event.peer->data = new int(clients.size());
    clients.push_back(c);
}

void Server::disconnected(ENetEvent &event)
{
    char *host = hostToStr(event.peer->address.host);
    int clientIdx = *((int*)event.peer->data);
    LOG(INFO) << "Client disconnected: " <<
        host <<
        ":" << event.peer->address.port;
    delete host;
    event.peer->data = NULL;
    clients.erase(clients.begin()+clientIdx);
}

void Server::stop()
{
    m_stop = true;
    if (m_thread.joinable())
        m_thread.join();
}

int Server::getPort()
{
    return m_server->address.port;
}

// const float timestep = 1.0f/120.0f;
//  accumulator += dt;
//  while (accumulator >= timestep)
//  {
//      GameState lastState = gameState;
//      // Do physics simulation by timestep
//      accumulator -= timestep;
//  }

//  const float t = accumulator/timestep;
//  GamePhysicsUtils::LerpState(interpolatedState, prevState, gameState, t);
