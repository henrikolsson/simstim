#ifndef _CLIENT_H
#define _CLIENT_H

#include <queue>
#include <vector>
#include "peer.h"
#include "client_callbacks.h"
#include "../common/util.h"
#include "../proto/simstim.pb.h"
#include "../proto/proto_types.h"

class Client : Peer
{
 public:
    Client();
    ~Client();
    bool connect(const char* host, int port);
    void disconnect();
    void hello();
    void work();
    std::queue<ENetPacket> packets;
    void handlePacket(ENetPeer *peer, uint32_t packetTyep, std::string &pkt);
    void setCallbacks(ClientCallbacks *cc);
    void setInput(int input);
    void getChunk(int32_t cx, int32_t cy, int32_t cz);
    uint64_t getPacketCounter();
    void setForward(glm::vec3 forward);
    void setRight(glm::vec3 right);
 private:
    ClientCallbacks *m_callbacks = nullptr;
    ENetHost *m_client;
    ENetPeer *m_peer;
    int32_t m_input = 0;
    float m_time_last = 0.0f;
    float m_time_last_second = 0.0f;
    uint64_t m_packets = 0;
    void handleSetPosition(std::string& buf);
    void handleChunkData(std::string& buf);
    std::vector<GetChunk *> m_chunkQueue;
    std::vector<GetChunk *> m_chunksPending;
    glm::vec3 m_forward;
    glm::vec3 m_right;
};
#endif
