#ifndef _CLIENT_CALLBACKS_H
#define _CLIENT_CALLBACKS_H

#include "../proto/simstim.pb.h"

class ClientCallbacks
{
 public:
    virtual ~ClientCallbacks() {}
    virtual void clientSetPosition(SetPosition *sp) = 0;
    virtual void clientChunkData(ChunkData *cd) = 0;
};

#endif
