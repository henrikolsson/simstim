#ifndef _PEER_H
#define _PEER_H

#include <string>
#include <sstream>
#include <cstdint>
#include <enet/enet.h>
#include <glog/logging.h>

#define NET_SEND(type, pkt) NET_SEND_PEER(m_peer, type, pkt);
#define NET_SEND_PEER(peer, type, pkt) do { \
std::string pkt_tmp_out; \
pkt.SerializeToString(&pkt_tmp_out); \
this->send(peer, type, pkt_tmp_out); \
} while (0)

class Peer
{
  public:
    Peer();
    virtual ~Peer();
  protected:
    void send(ENetPeer *peer, uint32_t packetType, std::string &pkt);
    void recv(ENetEvent &event);
    char *hostToStr(enet_uint32 host);
    virtual void handlePacket(ENetPeer *peer, uint32_t packetTyep, std::string &pkt) = 0;
};

#endif
