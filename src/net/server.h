#ifndef _SERVER_H
#define _SERVER_H

#include <math.h>
#include <stdexcept>
#include <thread>
#include <future>
#include <memory>
#include <mutex>
#include <unistd.h>
#include <enet/enet.h>
#include <glog/logging.h>
#include <vector>
#include <cstdint>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>
#include "peer.h"
#include "connection.h"
#include "../common/util.h"
#include "../common/config.h"
#include "../proto/simstim.pb.h"
#include "../proto/proto_types.h"

class OpenSimplexNoise;
#define SERVER_SEND_RATE 3
#define CLIENT_SEND_RATE 2
#define STEP_SIZE (1000.0f/60.0f)

#define DEFAULT_PORT 3333

#define ASSERT_STATUS(con, expected) if (con.state != expected)   \
    { \
        LOG(ERROR) << "Expected " << expected << ", not " << con.state; \
        return; \
    }

class Server : Peer
{
 public:
    Server(const char* host, const int port);
    Server() : Server("0.0.0.0",  ENET_PORT_ANY) {}
    ~Server();
    void stop();
    void run();
    void runAsync();
    int getPort();
    void handlePacket(ENetPeer *peer, uint32_t packetTyep, std::string &pkt);
    int getMaterial(int x, int y, int z);
 private:
    volatile bool m_stop = false;
    std::thread m_thread;
    std::mutex m_mutex;
    ENetAddress m_address;
    ENetHost *m_server;
    void connected(ENetEvent &event);
    void disconnected(ENetEvent &event);
    std::vector<connection> clients;
    void handleHello(std::string& buf, connection& con);
    void handleGetChunk(std::string& buf, connection& con);
    void handleMove(std::string& buf, connection& con);
    OpenSimplexNoise *m_noise;
};

#endif
